package com.pratham.messanger.recources;



import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.pratham.messanger.model.Message;
import com.pratham.messanger.service.MessageService;
/*
 * First we have mapped the URI to the class MessageResource
 * using @Path annotation
 * 
 * This is top level mapping
 * */
@Path("/messages")
public class MessageResource {
	
	MessageService messageService = new MessageService();
	
	/* 
	 * And now we are going to map a HTTP method to this Java method
	 * below we have mapped getMessage() using @GET annotation 
	 * @Produces specify the response format 
	 * Enumeration that lets us specify the different values
	 * 
	 * MediaType is enumeration that contains lot of standard content types
	 * that the HTTP response will return
	 * 
	 * TEXT_PLAIN is an indicator that the response is plane text
	 * */
	
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public List<Message> getMessages(){
		return messageService.getAllMessages();
	}
	
	/*
	 * Here we are making use of @Path annotation at method level
	 * 
	 * We also make use of @PathParam
	 * @PathParam - is a parameter annotation which allows you to map 
	 * variable URI path fragments into your method call. 
	 * 
	 * Example in below code @PathParam allows the value pass in @Path("/{messageId}")
	 * as parameter in method test() 
	 * 
	 * The value pass in {messageId} will be String but 
	 * Jersey will automatically convert it to long
	 * */
	
	@GET
	@Path("/{messageId}")
	@Produces(MediaType.APPLICATION_XML)
	public Message getMessage(@PathParam("messageId") long id){
		return messageService.getMessage(id);
	}
}
